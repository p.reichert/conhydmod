/* include R header file */
/* --------------------- */

#include <R.h>


/* definition of global variables and pointers to be initialized before call to ode */
/* -------------------------------------------------------------------------------- */

struct timedepvar {
                    int     n;                   /* length of vectors t and val */
                    double* t;                   /* time points */
                    double* val;                 /* values corresponding to the time points */
                  };

static int                global_npar     = 0;   /* number of parameters */
static struct timedepvar* global_par      = 0;   /* vector of constant or time-dependent parameters */
static char**             global_parnames = 0;   /* names of parameters */

static struct timedepvar  global_P = { .n = 0, .t=0, .val=0 };   /* time series of precipitation */
static struct timedepvar  global_E = { .n = 0, .t=0, .val=0 };   /* time series of potential evapotranspiration */


/* functions to store parameters and input into global variables */
/* (to be called with actual arguments in R before calling ode)  */
/* ------------------------------------------------------------- */

/* create global data structure for parameters */

void create_global_par_vector(int* npar, char** parnames)     /* number of parmeters */
{
   if ( global_npar != *npar )  /* remove and re-allocated memory */
   {
      /* free previously allocated memory */
      
      if ( global_npar > 0 )
      {  
         for ( int i=0; i<global_npar; i++)
         {
            free(global_par[i].t);
            free(global_par[i].val);
            free(global_parnames[i]);
         }
         free(global_par);
         free(global_parnames);
      }
      
      /* allocate new memory */
      
      global_npar = *npar;
      if ( *npar > 0 )
      {  
         global_par = (struct timedepvar*)malloc(*npar*sizeof(struct timedepvar));
         global_parnames = malloc(*npar*sizeof(char*));
         for ( int i=0; i<*npar; i++ ) 
         {
            global_par[i].n   = 0;
            global_par[i].t   = 0;
            global_par[i].val = 0;
            global_parnames[i] = malloc((strlen(parnames[i])+1)*sizeof(char));
            strcpy(global_parnames[i],parnames[i]);
         }
      }
   }
   else  /* check and potentially overwrite parameter names: */
   {
      if ( *npar > 0 )
      {  
         for ( int i=0; i<*npar; i++ ) 
         {
            if ( strcmp(global_parnames[i],parnames[i]) != 0 )
            {
               if ( strlen(global_parnames[i]) != strlen(parnames[i]) )
               {
                  free(global_parnames[i]);
                  global_parnames[i] = malloc((strlen(parnames[i])+1)*sizeof(char));
               }
               strcpy(global_parnames[i],parnames[i]);
            }
         }
      }
   }
}

/* save an individual parameters (to be called in a loop over parameters) */

void save_global_par(int*    ind,   /* parameter index, R convention (starting from 1) */
                     int*    n,     /* number of time points */
                     double* t,     /* vector of time points */
                     double* val)   /* vector of corresponding values */
{
   if ( *ind < 1 || *ind > global_npar ) error("save_global_par: parameter initialization with illegal index");
   if ( *n <= 0 )                        error("save_global_par: number of time points of parameter <= 0");
   
   if ( *n !=  global_par[*ind-1].n )
   {
      if ( global_par[*ind-1].n > 0 )
      {
         free(global_par[*ind-1].t);
         free(global_par[*ind-1].val);
      }
      global_par[*ind-1].n = *n;
      if ( *n > 0 )
      {
         global_par[*ind-1].t   = malloc(*n*sizeof(double));
         global_par[*ind-1].val = malloc(*n*sizeof(double));
      }
   }
   for ( int i=0; i<*n; i++ ) 
   {
      global_par[*ind-1].t[i]   = t[i];
      global_par[*ind-1].val[i] = val[i];
   }
}

/* save precipitation time series */

void save_global_P(int*    n,     /* number of time points */
                   double* t,     /* vector of time points */
                   double* val)   /* vector of corresponding values */
{
   if ( *n <= 0 ) error("save_global_P: number of time points of P <= 0");

   if ( *n != global_P.n )
   {
      if ( global_P.n > 0 )
      {
         free(global_P.t);
         free(global_P.val);
      }
      global_P.n = *n;
      if ( *n > 0 )
      {
         global_P.t   = malloc(*n*sizeof(double));
         global_P.val = malloc(*n*sizeof(double));
      }
   }
   for ( int i=0; i<*n; i++ ) 
   {
      global_P.t[i]   = t[i];
      global_P.val[i] = val[i];
   }
}

/* save potential evapotranspiration time series */

void save_global_E(int*    n,     /* number of time points */
                   double* t,     /* vector of time points */
                   double* val)   /* vector of corresponding values */
{
   if ( *n <= 0 ) error("save_global_E: number of time points of E <= 0");

   if ( *n !=  global_E.n )
   {
      if ( global_E.n > 0 )
      {
         free(global_E.t);
         free(global_E.val);
      }
      global_E.n = *n;
      if ( *n > 0 )
      {
         global_E.t   = malloc(*n*sizeof(double));
         global_E.val = malloc(*n*sizeof(double));
      }
   }
   for ( int i=0; i<*n; i++ ) 
   {
      global_E.t[i]   = t[i];
      global_E.val[i] = val[i];
   }
}

/* function to initialize parameters for the dynamic allocation model */

void init_rhs(void (* odeparms)(int *, double *))
{
   int    n = 1;
   double p = 0;
   odeparms(&n,&p);
}


/* auxiliary functions (linear interpolation and string search) */
/* ------------------------------------------------------------ */

/* linear interpolation */

double linint(double* t, double* val, int* n, double* tout)
{
   /* consistency checks */

   if ( *n <= 0 )                   error("linint: no elements in time-dependent variables");
   if ( *n > 1 && t[0] >= t[*n-1] ) error("linint: time must be increasing in time-dependent variables");
   
   /* special cases */
   
   if ( *n == 1 )          return(val[0]);
   if ( *tout <= t[0] )    return(val[0]);
   if ( *tout >= t[*n-1] ) return(val[*n-1]);
   
   /* search indices of points bounding interval containing t */
   
   int ind = floor((*tout-t[0])/(t[*n-1]-t[0])*(*n)); if ( ind < 0 ) ind = 0; if ( ind > *n-2 ) ind = *n-2;
   if ( *tout < t[ind] )
   {
      while ( *tout < t[ind] && ind > 0 ) { ind = ind-1; }
   }
   else
   {
      if ( *tout > t[ind+1] )
      {
         while ( *tout > t[ind+1] && ind < *n-2 ) { ind = ind+1; }
      }
   }
   if ( ind < 0 || ind >= *n ) error("linint: illegal index");
   if ( *tout<t[ind] || *tout>t[ind+1] ) error("linint: problem in interpolation");
   
   /* return internal interpolation result */

   return(((*tout-t[ind])*val[ind+1] + (t[ind+1]-*tout)*val[ind])/(t[ind+1]-t[ind]));
}

/* get index of character string */

int getind(char* word, char** words, int* n)
{
  if ( *n < 1 )  return(-1);
  for ( int i=0; i<*n; i++ )
  {
     if ( strcmp(word,words[i]) == 0 ) return(i);
  }
  return(-1);
}
   

/* hydrological models */
/* ------------------- */
 
/* one box model */

void rhs_conhydmod_1box(int *neq, double *t, double *y, double *ydot, 
                        double *yout, int *ip)
{
   if ( ip[0]<1 ) error("rhs_conhydmod_1box: nout should be at least 1");
   
   double P = linint(global_P.t,global_P.val,&global_P.n,t);
   double E = linint(global_E.t,global_E.val,&global_E.n,t);
   
   int ind_k     = getind("k"    ,global_parnames,&global_npar); if ( ind_k < 0 )     error("rhs_conhydmod_1box: parameter k is missing");
   int ind_alpha = getind("alpha",global_parnames,&global_npar); if ( ind_alpha < 0 ) error("rhs_conhydmod_1box: parameter alpha is missing");
   int ind_ce    = getind("ce"   ,global_parnames,&global_npar); if ( ind_ce < 0 )    error("rhs_conhydmod_1box: parameter ce is missing");
   int ind_m     = getind("m"    ,global_parnames,&global_npar); if ( ind_m < 0 )     error("rhs_conhydmod_1box: parameter m is missing");
   int ind_Styp  = getind("Styp" ,global_parnames,&global_npar); if ( ind_Styp < 0 )  error("rhs_conhydmod_1box: parameter Styp is missing");
   
   double k     = linint(global_par[ind_k].t    ,global_par[ind_k].val    ,&global_par[ind_k].n    ,t);
   double alpha = linint(global_par[ind_alpha].t,global_par[ind_alpha].val,&global_par[ind_alpha].n,t);
   double ce    = linint(global_par[ind_ce].t   ,global_par[ind_ce].val   ,&global_par[ind_ce].n   ,t);
   double m     = linint(global_par[ind_m].t    ,global_par[ind_m].val    ,&global_par[ind_m].n    ,t);
   double Styp  = linint(global_par[ind_Styp].t ,global_par[ind_Styp].val ,&global_par[ind_Styp].n ,t);
   
   int ind_f_k  = getind("f_k",global_parnames,&global_npar);
   if ( ind_f_k >= 0 )
   {
      double f_k = linint(global_par[ind_f_k].t,global_par[ind_f_k].val,&global_par[ind_f_k].n,t);
      k = f_k * k;
   }
   
   double S = y[0]; if ( S < 0 ) S = 0;
   double Q = k/pow(Styp,alpha-1)*pow(S,alpha);
   
   ydot[0] = P - Q - ce*E*(1-exp(-S/m));
   
   yout[0] = Q;
   yout[1] = P;
   yout[2] = E;
}

/* two box model */

void rhs_conhydmod_2box(int *neq, double *t, double *y, double *ydot, 
                        double *yout, int *ip)
{
   if ( ip[0]<1 ) error("rhs_conhydmod_2box: nout should be at least 1");
   
   double P = linint(global_P.t,global_P.val,&global_P.n,t);
   double E = linint(global_E.t,global_E.val,&global_E.n,t);
   
   int ind_k1      = getind("k1"     ,global_parnames,&global_npar); if ( ind_k1 < 0 )      error("rhs_conhydmod_2box: parameter k1 is missing");
   int ind_alpha1  = getind("alpha1" ,global_parnames,&global_npar); if ( ind_alpha1 < 0 )  error("rhs_conhydmod_2box: parameter alpha1 is missing");
   int ind_k2      = getind("k2"     ,global_parnames,&global_npar); if ( ind_k2 < 0 )      error("rhs_conhydmod_2box: parameter k2 is missing");
   int ind_alpha2  = getind("alpha2" ,global_parnames,&global_npar); if ( ind_alpha2 < 0 )  error("rhs_conhydmod_2box: parameter alpha2 is missing");
   int ind_k12     = getind("k12"    ,global_parnames,&global_npar); if ( ind_k12 < 0 )     error("rhs_conhydmod_2box: parameter k12 is missing");
   int ind_alpha12 = getind("alpha12",global_parnames,&global_npar); if ( ind_alpha12 < 0 ) error("rhs_conhydmod_2box: parameter alpha12 is missing");
   int ind_ce      = getind("ce"     ,global_parnames,&global_npar); if ( ind_ce < 0 )      error("rhs_conhydmod_2box: parameter ce is missing");
   int ind_m1      = getind("m1"     ,global_parnames,&global_npar); if ( ind_m1 < 0 )      error("rhs_conhydmod_2box: parameter m1 is missing");
   int ind_m2      = getind("m2"     ,global_parnames,&global_npar); if ( ind_m2 < 0 )      error("rhs_conhydmod_2box: parameter m2 is missing");
   int ind_Styp    = getind("Styp"   ,global_parnames,&global_npar); if ( ind_Styp < 0 )    error("rhs_conhydmod_2box: parameter Styp is missing");
   
   double k1      = linint(global_par[ind_k1].t     ,global_par[ind_k1].val     ,&global_par[ind_k1].n     ,t);
   double alpha1  = linint(global_par[ind_alpha1].t ,global_par[ind_alpha1].val ,&global_par[ind_alpha1].n ,t);
   double k2      = linint(global_par[ind_k2].t     ,global_par[ind_k2].val     ,&global_par[ind_k2].n     ,t);
   double alpha2  = linint(global_par[ind_alpha2].t ,global_par[ind_alpha2].val ,&global_par[ind_alpha2].n ,t);
   double k12     = linint(global_par[ind_k12].t    ,global_par[ind_k12].val    ,&global_par[ind_k12].n    ,t);
   double alpha12 = linint(global_par[ind_alpha12].t,global_par[ind_alpha12].val,&global_par[ind_alpha12].n,t);
   double ce      = linint(global_par[ind_ce].t     ,global_par[ind_ce].val     ,&global_par[ind_ce].n     ,t);
   double m1      = linint(global_par[ind_m1].t     ,global_par[ind_m1].val     ,&global_par[ind_m1].n     ,t);
   double m2      = linint(global_par[ind_m2].t     ,global_par[ind_m2].val     ,&global_par[ind_m2].n     ,t);
   double Styp    = linint(global_par[ind_Styp].t   ,global_par[ind_Styp].val   ,&global_par[ind_Styp].n   ,t);
   
   int ind_f_k1  = getind("f_k1",global_parnames,&global_npar);
   if ( ind_f_k1 >= 0 )
   {
      double f_k1 = linint(global_par[ind_f_k1].t,global_par[ind_f_k1].val,&global_par[ind_f_k1].n,t);
      k1 = f_k1 * k1;
   }
   int ind_f_k2  = getind("f_k2",global_parnames,&global_npar);
   if ( ind_f_k2 >= 0 )
   {
      double f_k2 = linint(global_par[ind_f_k2].t,global_par[ind_f_k2].val,&global_par[ind_f_k2].n,t);
      k2 = f_k2 * k2;
   }
   int ind_f_k12  = getind("f_k12",global_parnames,&global_npar);
   if ( ind_f_k12 >= 0 )
   {
      double f_k12 = linint(global_par[ind_f_k12].t,global_par[ind_f_k12].val,&global_par[ind_f_k12].n,t);
      k12 = f_k12 * k12;
   }
   
   double S1  = y[0]; if ( S1 < 0 ) S1 = 0;
   double S2  = y[1]; if ( S2 < 0 ) S2 = 0;
   double Q1  = k1/pow(Styp,alpha1-1)*pow(S1,alpha1);
   double Q2  = k2/pow(Styp,alpha2-1)*pow(S2,alpha2);
   double Q12 = k12/pow(Styp,alpha12-1)*pow(S1,alpha12);
   
   ydot[0] = P - Q1 - Q12 - ce*E*(1-exp(-S1/m1));
   ydot[1] = Q12 - Q2     - ce*E*exp(-S1/m1)*(1-exp(-S2/m2));
   
   yout[0] = Q1 + Q2;
   yout[1] = P;
   yout[2] = E;
}

/* SIP model */

double xi_to_x(double *xi)
{
  double a, b, c, gamma;
  double xi_1 = -1.736;
  double xi_2 =  1.079;
  if ( *xi <= xi_1 ) return(0);
  if ( *xi >= xi_2 ) 
  {
    a     =  0.001;
    b     = -1.022;
    c     =  0.074;
    gamma =  4.435;
  }
  else
  {
    a     =  0.02;
    b     = -1.736;
    c     =  0;
    gamma =  1.561;
  }
  return(a*pow(*xi-b,gamma) + c);
}

void rhs_conhydmod_SIP(int *neq, double *t, double *y, double *ydot, 
                       double *yout, int *ip)
{
   double pi = 3.141592653589;	

   if ( ip[0]<1 ) error("rhs_conhydmod_SIP: nout should be at least 1");
   
   int ind_A     = getind("A"    ,global_parnames,&global_npar); if ( ind_A     < 0 ) error("rhs_conhydmod_SIP: parameter A is missing");
   int ind_K     = getind("K"    ,global_parnames,&global_npar); if ( ind_K     < 0 ) error("rhs_conhydmod_SIP: parameter K is missing");
   int ind_x_gw  = getind("x_gw" ,global_parnames,&global_npar); if ( ind_x_gw  < 0 ) error("rhs_conhydmod_SIP: parameter x_gw is missing");
   int ind_zeta1 = getind("zeta1",global_parnames,&global_npar); if ( ind_zeta1 < 0 ) error("rhs_conhydmod_SIP: parameter zeta1 is missing");
   int ind_zeta2 = getind("zeta2",global_parnames,&global_npar); if ( ind_zeta2 < 0 ) error("rhs_conhydmod_SIP: parameter zeta2 is missing");
   int ind_chi1  = getind("chi1" ,global_parnames,&global_npar); if ( ind_chi1  < 0 ) error("rhs_conhydmod_SIP: parameter chi1 is missing");
   int ind_chi2  = getind("chi2" ,global_parnames,&global_npar); if ( ind_chi2  < 0 ) error("rhs_conhydmod_SIP: parameter chi2 is missing");
   int ind_xi    = getind("xi"   ,global_parnames,&global_npar); if ( ind_xi    < 0 ) error("rhs_conhydmod_SIP: parameter xi is missing");
   
   double A     = linint(global_par[ind_A].t    ,global_par[ind_A].val    ,&global_par[ind_A].n    ,t);
   double K     = linint(global_par[ind_K].t    ,global_par[ind_K].val    ,&global_par[ind_K].n    ,t);
   double x_gw  = linint(global_par[ind_x_gw].t ,global_par[ind_x_gw].val ,&global_par[ind_x_gw].n ,t);
   double zeta1 = linint(global_par[ind_zeta1].t,global_par[ind_zeta1].val,&global_par[ind_zeta1].n,t);
   double zeta2 = linint(global_par[ind_zeta2].t,global_par[ind_zeta2].val,&global_par[ind_zeta2].n,t);
   double chi1  = linint(global_par[ind_chi1].t ,global_par[ind_chi1].val ,&global_par[ind_chi1].n ,t);
   double chi2  = linint(global_par[ind_chi2].t ,global_par[ind_chi2].val ,&global_par[ind_chi2].n ,t);
   double xi    = linint(global_par[ind_xi].t   ,global_par[ind_xi].val   ,&global_par[ind_xi].n   ,t);
   
   double P = xi_to_x(&xi);
   
   double S = y[0]; if ( S < 0 ) S = 0;
   double Q = S/K*1000/3600 + zeta1*sin(2*pi*(*t)/24) + chi1*cos(2*pi*(*t)/24) + zeta2*sin(2*pi*(*t)/12) + chi2*cos(2*pi*(*t)/12);
   
   ydot[0] = A*P/1000*60 + x_gw/1000*3600 - S/K;
   
   yout[0] = Q;
   yout[1] = P;
   yout[2] = 0;
}



