# scaled t distribution:
# ======================

# density function:

dt.scaled <- function(x,df,sigma,log=FALSE)
{
  d <- rep(NA,length(x))
  if ( sigma > 0 & df > 2 )
  {
    fact <- sqrt(df/(df-2)) / sigma
    if ( log )
    {
      d <- log(fact) + dt(fact*x,df,log=TRUE)
    }
    else
    {
      d <- fact * dt(fact*x,df,log=FALSE)
    }
  }
  return(d)
}

# cumulative distribution function:

pt.scaled <- function(q,df,sigma,log.p=FALSE)
{
  p <- rep(NA,length(q))
  if ( sigma > 0 & df > 2 )
  {
    fact <- sqrt(df/(df-2)) / sigma
    p <- pt(fact*q,df,log.p=log.p)
  }
  return(p)
}

# inverse cumulative distribution function:

qt.scaled <- function(p,df,sigma,log.p=FALSE)
{
  q <- rep(NA,length(p))
  if ( sigma > 0 & df > 2 )
  {
    fact <- sqrt(df/(df-2)) / sigma
    if ( log.p )
    {
      p.loc <- ifelse(p>1,NA,p)
      q <- qt(p.loc,df,log.p=TRUE) - log(fact)
    }
    else
    {
      p.loc <- ifelse(p<0,NA,ifelse(p>1,NA,p))
      q <- qt(p.loc,df,log.p=FALSE)/fact
    }
  }
  return(q)
}

# random numbers generating function:

rt.scaled <- function(n,df,sigma)
{
  r <- rep(NA,n)
  if ( sigma > 0 & df > 2 )
  {
    r <- qt.scaled(runif(n),df=df,sigma=sigma)
  }
  return(r)
}


# skewed, scaled t distribution:
# ==============================

# conversion of sd of skewed distribution to sd of t distribution that is made skewed:

sigma2sigma.t <- function(sigma,df,gamma)
{
  return(sigma / sqrt( ( gamma^3 + 1/gamma^3 ) / ( gamma + 1/gamma ) 
                       - 4 * ( ( gamma^2 - 1/gamma^2 ) / ( gamma + 1/gamma ) )^2 
                       * df*(df-2)/(df-1)^2 * gamma((df+1)/2)^2 / (pi*df*gamma(df/2)^2) ))
}

# density function:

dt.skewed <- function(x,df,sigma,gamma,log=FALSE)
{
  d <- rep(NA,length(x))
  if ( sigma > 0 & df > 2 & gamma > 0 )
  {
    # convert sigma of the skewed distribution to sigma of the scaled t distribution
    # that is made skewed with the scaling gamma and 1/gamma:
    sigma.t <- sigma2sigma.t(sigma,df,gamma)
    
    # calculate density based on the density of the scaled t distribution:
    fact <- 2 / (gamma + 1/gamma)
    if ( log )
    {
      d <- log(fact) + ifelse(x <= 0,
                              dt.scaled(x*gamma,df,sigma.t,log=TRUE),
                              dt.scaled(x/gamma,df,sigma.t,log=TRUE))
    }
    else
    {
      d <- fact * ifelse(x <= 0,
                         dt.scaled(x*gamma,df,sigma.t,log=FALSE),
                         dt.scaled(x/gamma,df,sigma.t,log=FALSE))
    }
  }
  return(d)
}

# cumulative distribution function:

pt.skewed <- function(q,df,sigma,gamma)
{
  p <- rep(NA,length(q))
  if ( sigma > 0 & df > 2 & gamma > 0 )
  {
    # convert sigma of the skewed distribution to sigma of the scaled t distribution
    # that is made skewed with the scaling gamma and 1/gamma:
    sigma.t <- sigma2sigma.t(sigma,df,gamma)
    
    # calculated cdf based on the cdf of the scaled t distribution:
    fact <- 2 / (1 + gamma^2)
    p <- fact * ifelse(q <= 0,
                       pt.scaled(q*gamma,df,sigma.t),
                       1/2 + gamma^2*(pt.scaled(q/gamma,df,sigma.t)-1/2))
  }
  return(p)
}

# complementary cumulative distribution function (1-cdf):

cpt.skewed <- function(q,df,sigma,gamma)
{
  p <- rep(NA,length(q))
  if ( sigma > 0 & df > 2 & gamma > 0 )
  {
    # convert sigma of the skewed distribution to sigma of the scaled t distribution
    # that is made skewed with the scaling gamma and 1/gamma:
    sigma.t <- sigma2sigma.t(sigma,df,gamma)
    
    # calculated cdf based on the cdf of the scaled t distribution:
    fact <- 2 / (1 + 1/gamma^2)
    p <- fact * ifelse(q <= 0,
                       1/2 + 1/gamma^2*(pt.scaled(-q*gamma,df,sigma.t)-1/2),
                       pt.scaled(-q/gamma,df,sigma.t))
  }
  return(p)
}

# inverse cumulative distribution function:

qt.skewed <- function(p,df,sigma,gamma)
{
  q <- rep(NA,length(p))
  if ( sigma > 0 & df > 2 & gamma > 0 )
  {
    # convert sigma of the skewed distribution to sigma of the scaled t distribution
    # that is made skewed with the scaling gamma and 1/gamma:
    sigma.t <- sigma2sigma.t(sigma,df,gamma)
    
    # calculated cdf based on the cdf of the scaled t distribution:
    fact <- 2 / (1 + gamma^2)
    q <- ifelse(p <= 0.5*fact,
                qt.scaled(p/fact,df,sigma.t)/gamma,
                qt.scaled((p/fact-1/2)/gamma^2+1/2,df,sigma.t)*gamma)
  }
  return(q)
}

# random numbers generating function:

rt.skewed <- function(n,df,sigma,gamma)
{
  r <- rep(NA,n)
  if ( sigma > 0 & df > 2 & gamma > 0 )
  {
    r <- qt.skewed(runif(n),df=df,sigma=sigma,gamma=gamma)
  }
  return(r)
}



